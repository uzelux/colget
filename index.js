/**
 * Get the value specified in path, throws error if value not accessible or undefined
 * @param   {Object|Array}  object  - Object to be searched
 * @param   {String}        [path]  - Path to value in dot notation
 * @return  {*}                     - Target Value
 * @throws {ReferenceError} Will throw reference error if value is not accessible or undefined
 */
module.exports.getOrFail = (object, path) => {
  if (path.constructor.name !== 'String') throw new TypeError(`Variable path is expected to be String, got ${path.constructor.name} instead`);

  if (!path || path.length === 0 || !object) return object;

  return path.split('.').reduce((output, element) => {
    if (output[element]) return output[element];
    throw new ReferenceError(`Element ${element} does not exist or not defined  in object`);
  }, object)
};

/**
 * Get the value specified in path, and return fallback value if value not accessible or undefined
 * @param   {Object|Array}  object                - Object to be searched
 * @param   {String}        [path]                - Path to value in dot notation
 * @param   {*}             [fallback=undefined]  - Default Value if target value is not accessible or undefined
 * @return  {*}                                   - Target Value
 */
module.exports.get = (object, path, fallback) => {
  if (path.constructor.name !== 'String') throw new TypeError(`Variable path is expected to be String, got ${path.constructor.name} instead`);

  if (!path || path.length === 0 || !object) return object;

  try {
    return path.split('.').reduce((output, element) => {
      if (output[element]) {return output[element];}
      throw new ReferenceError(`Element ${element} does not exist or not defined  in object`);
    }, object)
  } catch (e) {
    return fallback;
  }
};

/**
 * Get the value specified in path, throws error if value not accessible
 * @param   {Object|Array}  object  - Object to be searched
 * @param   {String}        [path]  - Path to value in dot notation
 * @return  {*}                     - Target Value
 * @throws {ReferenceError} Will throw reference error if value is not accessible
 */
module.exports.valueOrFail = (object, path) => {
  if (path.constructor.name !== 'String') throw new TypeError(`Variable path is expected to be String, got ${path.constructor.name} instead`);

  if (!path || path.length === 0 || !object) return object;

  return path.split('.').reduce((output, element) => {
    if (output.hasOwnProperty(element)) return output[element];
    throw new ReferenceError(`Element ${element} does not exist in object`);
  }, object)
};

/**
 * Get the value specified in path, and return fallback value if value not accessible
 * @param   {Object|Array}  object                - Object to be searched
 * @param   {String}        [path]                - Path to value in dot notation
 * @param   {*}             [fallback=undefined]  - Default Value if target value is not accessible or undefined
 * @return  {*}                                   - Target Value
 */
module.exports.value = (object, path, fallback) => {
  if (path.constructor.name !== 'String') throw new TypeError(`Variable path is expected to be String, got ${path.constructor.name} instead`);

  if (!path || path.length === 0 || !object) return object;

  try {
    return path.split('.').reduce((output, element) => {
      if (output.hasOwnProperty(element)) return output[element];
      throw new ReferenceError(`Element ${element} does not exist in object`);
    }, object)
  } catch (e) {
    return fallback;
  }
};

/**
 * Check the value specified in path accessible and defined or not, throws error if not
 * @param   {Object|Array}  object  - Object to be searched
 * @param   {String}        [path]  - Path to value in dot notation
 * @return  {*}                     - Target Value
 * @throws {ReferenceError} Will throw reference error if value is not accessible
 */
module.exports.hasValueOrFail = (object, path) => {
  if (path.constructor.name !== 'String') throw new TypeError(`Variable path is expected to be String, got ${path.constructor.name} instead`);

  if (!path || path.length === 0 || !object) return object;

  return !!path.split('.').reduce((output, element) => {
    if (output[element]) return output[element];
    throw new ReferenceError(`Element ${element} does not exist or not defined  in object`);
  }, object)
};

/**
 * Check the value specified in path accessible and defined or not
 * @param   {Object|Array}  object  - Object to be searched
 * @param   {String}        [path]  - Path to value in dot notation
 * @return  {*}                     - Target Value
 */
module.exports.hasValue = (object, path) => {
  if (path.constructor.name !== 'String') throw new TypeError(`Variable path is expected to be String, got ${path.constructor.name} instead`);

  if (!path || path.length === 0 || !object) return object;

  try {
    return !!path.split('.').reduce((output, element) => {
      if (output[element]) return output[element];
      throw new ReferenceError(`Element ${element} does not exist or not defined in object`);
    }, object)
  } catch (e) {
    return false;
  }
};

/**
 * Check the value specified in path accessible or not, throws error if not
 * @param   {Object|Array}  object  - Object to be searched
 * @param   {String}        [path]  - Path to value in dot notation
 * @return  {*}                     - Target Value
 * @throws {ReferenceError} Will throw reference error if value is not accessible
 */
module.exports.hasOrFail = (object, path) => {
  if (path.constructor.name !== 'String') throw new TypeError(`Variable path is expected to be String, got ${path.constructor.name} instead`);

  if (!path || path.length === 0 || !object) return object;

  const { exist } = path.split('.').reduce(({ exist, output }, element) => {
    if (output.hasOwnProperty(element)) return { exist, output: output[element] };
    throw new ReferenceError(`Element ${element} does not exist in object`);
  }, { exist: true, output: object });
  return exist;
};

/**
 * Check the value specified in path accessible or not
 * @param   {Object|Array}  object  - Object to be searched
 * @param   {String}        [path]  - Path to value in dot notation
 * @return  {*}                     - Target Value
 */
module.exports.has = (object, path) => {
  if (path.constructor.name !== 'String') throw new TypeError(`Variable path is expected to be String, got ${path.constructor.name} instead`);

  if (!path || path.length === 0 || !object) return object;

  try {
    const { exist } = path.split('.').reduce(({ exist, output }, element) => {
      if (output.hasOwnProperty(element)) return { exist, output: output[element] };
      throw new ReferenceError(`Element ${element} does not exist in object`);
    }, { exist: true, output: object });
    return exist;
  } catch (e) {
    return false;
  }
};

/**
 * Get all values which key matches the regex tree provided
 * @param {Object}      object  - Object to be searched
 * @param {...RegExp}   regexes - Regular expressions to be check against
 * @return {Object[]}           - Target Values
 */
module.exports.regexp = (object, ...regexes) => {
  if (!regexes || regexes.length === 0 || !object) return object;

  return regexes
    .reduce((output, path) => {
      return output
        .flatMap(([_, value]) => Object.entries(value))
        .filter(([key]) => path.test(key));
    }, [[undefined, object]])
    .map(([key, value]) => ({ [key]: value }));
};
