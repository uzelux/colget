# Collection Get!

This is just a mini-helper to help you retrieve nested attributes!

<details>
<summary>CHANGE LOG</summary>

V 2.3.1
- Added Regular Expression Handling!

V 2.2.1
- Fixed Getter not returning anything

V 2.2.0
- Included Has/Exist functions!
- Added Special Handling for null and undefined values!
- Added JSDocs!

V. 2.1.2
- Minor update on exporting function

V. 2.1.1
- We have Git Now!

V. 2.1.0
- Performance Update
- Update default value for default in get() to undefined
</details>

---

## Usage
```javascript
const ColGet = require('@uzelux/colget');
```

Sample Object
```javascript
const sample = {
  foo: 'bar',
  baz: { john: 'doe' },
  nullValue: null,
  undefinedValue: undefined,
};
```

<details>
<summary> Retrieve Values (discard null and undefined values) </summary>

```javascript
ColGet.get(sample, 'foo');
// 'bar'

ColGet.get(sample, 'baz.john');
// 'doe'

ColGet.get(sample, 'baz');
// {john: 'doe'}

ColGet.get(sample, 'non.exist');
// undefined

ColGet.get(sample, 'nullValue');
// undefined

ColGet.get(sample, 'undefinedValue');
// undefined

ColGet.get(sample, 'non.exist', 'default');
// 'default'

ColGet.getOrFail(sample, 'non.exist');
// ReferenceError: Element non does not exist in object;

ColGet.getOrFail(sample, 'nullValue');
// ReferenceError: Element nullValue does not exist in object;

ColGet.getOrFail(sample, 'undefinedValue');
// ReferenceError: Element undefinedValue does not exist in object;
```
</details>

<details>
<summary> Retrieve Values (persist null and undefined values)</summary>

```javascript
ColGet.value(sample, 'foo');
// 'bar'

ColGet.value(sample, 'baz.john');
// 'doe'

ColGet.value(sample, 'baz');
// {john: 'doe'}

ColGet.value(sample, 'non.exist');
// undefined

ColGet.value(sample, 'nullValue');
// null

ColGet.value(sample, 'undefinedValue');
// undefined

ColGet.value(sample, 'non.exist', 'default');
// 'default'

ColGet.valueOrFail(sample, 'non.exist');
// ReferenceError: Element non does not exist in object;

ColGet.valueOrFail(sample, 'nullValue');
// null

ColGet.valueOrFail(sample, 'undefinedValue');
// undefined
```
</details>

<details>
<summary> Retrieve Values with RegExp</summary>

```javascript
ColGet.regexp(sample, /foo/);
// [ {foo: 'bar'} ]

ColGet.regexp(sample, /baz/, /j\w+/);
// [ {john: 'doe'} ]

ColGet.regexp(sample, /BAZ/i);
// [ {baz: {john: 'doe'}} ]

ColGet.regexp(sample, /nonexist/);
// []

```
</details>

<details>
<summary> Check Accessible and Defined</summary>

```javascript
ColGet.hasValue(sample, 'foo');
// true

ColGet.hasValue(sample, 'baz.john');
// true

ColGet.hasValue(sample, 'baz');
// true

ColGet.hasValue(sample, 'non.exist');
// false

ColGet.hasValue(sample, 'nullValue');
// false

ColGet.hasValue(sample, 'undefinedValue');
// false

ColGet.hasValueOrFail(sample, 'non.exist');
// ReferenceError: Element non does not exist in object;

ColGet.hasValueOrFail(sample, 'nullValue');
// ReferenceError: Element nullValue does not exist in object;

ColGet.hasValueOrFail(sample, 'undefinedValue');
// ReferenceError: Element undefinedValue does not exist in object;
```
</details>

<details>
<summary> Check Accessible</summary>

```javascript
ColGet.has(sample, 'foo');
// true

ColGet.has(sample, 'baz.john');
// true

ColGet.has(sample, 'baz');
// true

ColGet.has(sample, 'non.exist');
// false

ColGet.has(sample, 'nullValue');
// true

ColGet.has(sample, 'undefinedValue');
// true

ColGet.hasOrFail(sample, 'non.exist');
// ReferenceError: Element non does not exist in object;

ColGet.hasOrFail(sample, 'nullValue');
// true

ColGet.hasOrFail(sample, 'undefinedValue');
// true
```
</details>
